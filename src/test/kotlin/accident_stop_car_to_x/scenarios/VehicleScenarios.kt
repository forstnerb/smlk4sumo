package accident_stop_car_to_x.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.AccidentZoneController
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.sumo.expect

val triggerVehicleSlowDownInDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{

    val SPEED_IN_DANGER_ZONE = 5.00

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    // makes the vehicles not change lanes by themselves/ because of sumo anymore
    request(vehicleController.setLCM(512))
    request(vehicleController.setSpeed(SPEED_IN_DANGER_ZONE))

    waitFor(accidentZoneController.vehicleExitedDangerZone(vehicleController))

    // sets the speed back to what it was before
    request(vehicleController.setSpeed(-1.00))
}

val vehicleMustSlowDownInDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_SLOW_DOWN = 30

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(accidentZoneController.vehicleExitedDangerZone(vehicleController))

    expect(
        {vehicleController.speed <= SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_SLOW_DOWN,
        "Vehicle " + vehicleController.id + " must slow down in DangerZone of " + accidentZoneController.id
    )
}

val triggerVehicleStopInStopArea = scenario(AccidentZoneController::vehicleEnteredFilledStopArea.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(accidentZoneController.vehicleController.resumed())

    // make the vehicle stop
    request(vehicleController.setSpeed(0.00))

    waitFor(accidentZoneController.giveVehiclePermissionToPass(vehicleController))

    // make the vehicle resume
    request(vehicleController.setSpeed(SPEED_IN_DANGER_ZONE))
    // allow the vehicle to change lanes by itself again
    request(vehicleController.setLCM(-1))
}

val triggerVehicleNoStopInStopArea = scenario(AccidentZoneController::vehicleEnteredEmptyStopArea.symbolicEvent())
{
    val vehicleController = it.parameters[0] as VehicleController

    // allow the vehicle to change lanes by itself again
    request(vehicleController.setLCM(-1))
}

val vehicleMustPassAfterEnteringEmptyStopArea = scenario(AccidentZoneController::vehicleEnteredEmptyStopArea.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA = 165

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA + " steps (since entering the empty StopArea)."
    )

}

val vehicleMustPassAfterEnteringFilledStopArea = scenario(AccidentZoneController::giveVehiclePermissionToPass.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA = 170

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA + " steps (since entering the filled StopArea)."
    )

}

val vehicleMustPassAccidentAfterEnteringDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE = 1000

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE + " steps (since entering the DangerZone)."
    )
}