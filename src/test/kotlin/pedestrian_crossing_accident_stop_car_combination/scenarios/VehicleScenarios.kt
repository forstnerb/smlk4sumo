package pedestrian_crossing_accident_stop_car_combination.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.AccidentZoneController
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightController
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightPhase
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.sumo.expect
import org.scenariotools.smlk4sumo.sumo.urgent
import org.scenariotools.smlk4sumo.sumo.waitForSimulationSteps

val vehicleNotifiesTLControllerOnApproach = scenario(VehicleController::enteredOuterTLPerimeter.symbolicEvent())
{
    val vehicleController = it.receiver
    val trafficLightController = it.parameters[0] as TrafficLightController

    urgent(trafficLightController.vehicleApproaching(vehicleController))

}

val triggerVehiclesGreenUponVehicleRequest = scenario(TrafficLightController::vehicleApproaching.symbolicEvent()) {

    val trafficLightController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)

    if (trafficLightController.currentPhase != TrafficLightPhase.vehicleGreen) {
        // if not green, then request green
        //println("*** requesting green for vehicles...")
        request(trafficLightController.turn(TrafficLightPhase.vehicleGreen))
        //println("*** requestED green for vehicles...")
    } else {
        // if green for vehicles for less than 15 seconds, keep green for cars, otherwise allow other transitionToPhase requests.
        if (trafficLightController.currentPhaseActiveSeconds <= 15) {
            interruptingEvents.add(trafficLightController.currentPhasedIsActiveFor(15))

            urgent(
                vehicleController.timeKeepingGreen(
                    trafficLightController,
                    15 - trafficLightController.currentPhaseActiveSeconds
                )
            )

            //println("***** keep green for vehicle ${vehicleController.id}...")
            forbiddenEvents.add(trafficLightController receives TrafficLightController::turn)
            waitFor(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)
        }
    }
}


val vehiclesGreenMustOccurAfterVehicleRequest = scenario(TrafficLightController::vehicleApproaching.symbolicEvent())
{

    val MAXSTEPSUNTILGREEN = 300 // 30 sec.

    val trafficLightController = it.receiver

    interruptingEvents.add(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)

    expect(
        { trafficLightController.currentPhase == TrafficLightPhase.vehicleGreen },
        MAXSTEPSUNTILGREEN,
        "Vehicles approaching the traffic light must have green after $MAXSTEPSUNTILGREEN steps."
    )
}

val vehiclesMustHaveGreenFor15Sec =
    scenario(TrafficLightController::phaseChanged.symbolicEvent() param TrafficLightPhase.vehicleGreen) {

        val minDelayBetweenGreenForVehicles = 150 // steps of 0.1 sec = 15 sec.

        val trafficLightController = it.receiver

        forbiddenEvents.add(trafficLightController receives TrafficLightController::turn)

        waitForSimulationSteps(minDelayBetweenGreenForVehicles)

    }

val triggerVehicleSlowDownInDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{

    val SPEED_IN_DANGER_ZONE = 5.00

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    // makes the vehicles not change lanes by themselves/ because of sumo anymore
    request(vehicleController.setLCM(512))
    request(vehicleController.setSpeed(SPEED_IN_DANGER_ZONE))

    waitFor(accidentZoneController.vehicleExitedDangerZone(vehicleController))

    // sets the speed back to what it was before
    request(vehicleController.setSpeed(-1.00))
}

val vehicleMustSlowDownInDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_SLOW_DOWN = 30

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(accidentZoneController.vehicleExitedDangerZone(vehicleController))

    expect(
        {vehicleController.speed <= SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_SLOW_DOWN,
        "Vehicle " + vehicleController.id + " must slow down in DangerZone of " + accidentZoneController.id
    )
}

val triggerVehicleStopInStopArea = scenario(AccidentZoneController::vehicleEnteredFilledStopArea.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(accidentZoneController.vehicleController.resumed())

    // make the vehicle stop
    request(vehicleController.setSpeed(0.00))

    waitFor(accidentZoneController.giveVehiclePermissionToPass(vehicleController))

    // make the vehicle resume
    request(vehicleController.setSpeed(SPEED_IN_DANGER_ZONE))
    // allow the vehicle to change lanes by itself again
    request(vehicleController.setLCM(-1))
}

val triggerVehicleNoStopInStopArea = scenario(AccidentZoneController::vehicleEnteredEmptyStopArea.symbolicEvent())
{
    val vehicleController = it.parameters[0] as VehicleController

    // allow the vehicle to change lanes by itself again
    request(vehicleController.setLCM(-1))
}

val vehicleMustPassAfterEnteringEmptyStopArea = scenario(AccidentZoneController::vehicleEnteredEmptyStopArea.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA = 165

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_EMPTY_SA + " steps (since entering the empty StopArea)."
    )

}

val vehicleMustPassAfterEnteringFilledStopArea = scenario(AccidentZoneController::giveVehiclePermissionToPass.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA = 170

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_FILLED_SA + " steps (since entering the filled StopArea)."
    )

}

val vehicleMustPassAccidentAfterEnteringDangerZone = scenario(AccidentZoneController::vehicleEnteredDangerZone.symbolicEvent())
{
    val SPEED_IN_DANGER_ZONE = 5.00
    val MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE = 1000

    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    expect(
        {vehicleController.speed > SPEED_IN_DANGER_ZONE},
        MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE,
        "Vehicle " + vehicleController.id + " must leave the DangerZone of " + accidentZoneController.id +
                " after " + MAX_STEPS_TO_PASS_ACCIDENT_AFTER_ENTERING_DANGERZONE + " steps (since entering the DangerZone)."
    )
}