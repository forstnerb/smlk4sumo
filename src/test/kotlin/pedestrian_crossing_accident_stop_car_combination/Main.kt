package pedestrian_crossing_accident_stop_car_combination

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.CompositeScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.createControllerInterfaceScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerOutputSymbolicEvents
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerInputSymbolicEvents
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerTerminatingEvents
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import pedestrian_crossing_accident_stop_car_combination.scenarios.*


// testing
fun main(){

    val compositeScenarioProgram = CompositeScenarioProgram()


    val scenarioProgramSumoLayer = createSumoScenarioProgram(
        "sumo-gui.exe",
        "C:\\Prog\\IntelliJ\\smlk4sumo\\src\\test\\resources\\pedestrian_crossing_accident_stop_car_combination\\data\\run.sumocfg",
        0.1
    )
    val scenarioProgramV2XControllerInterfaceLayer = createControllerInterfaceScenarioProgram()

    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerPedestriansGreenUponPedestrianApproaching)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(pedestriansMustHaveGreenFor15Sec)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(pedestriansGreenMustOccurAfterPedestrianApproaching)


    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleNotifiesTLControllerOnApproach)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerVehiclesGreenUponVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehiclesGreenMustOccurAfterVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehiclesMustHaveGreenFor15Sec)

    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeVehicleApproachingEvent)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeTimeKeepingGreenForVehicleEvent)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeVehicleControllerInputEvents)

    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerVehicleSlowDownInDangerZone)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleMustSlowDownInDangerZone)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerVehicleStopInStopArea)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerVehicleNoStopInStopArea)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleMustPassAfterEnteringEmptyStopArea)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleMustPassAfterEnteringFilledStopArea)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleMustPassAccidentAfterEnteringDangerZone)


    compositeScenarioProgram.addScenarioProgram(
        scenarioProgramSumoLayer,
        scenarioProgramV2XControllerInterfaceLayer
    )


    scenarioProgramV2XControllerInterfaceLayer.eventNode.registerSender(scenarioProgramSumoLayer.eventNode, sumoLayerOutputSymbolicEvents, sumoLayerTerminatingEvents)
    scenarioProgramSumoLayer.eventNode.registerSender(scenarioProgramV2XControllerInterfaceLayer.eventNode, sumoLayerInputSymbolicEvents)


    runBlocking {
        compositeScenarioProgram.run()
    }

}