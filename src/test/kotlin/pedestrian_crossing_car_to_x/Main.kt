package pedestrian_crossing_car_to_x

import kotlinx.coroutines.runBlocking
import org.scenariotools.smlk.CompositeScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.createControllerInterfaceScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerOutputSymbolicEvents
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerInputSymbolicEvents
import org.scenariotools.smlk4sumo.controllersinterface.sumoLayerTerminatingEvents
import org.scenariotools.smlk4sumo.sumo.createSumoScenarioProgram
import pedestrian_crossing_car_to_x.scenarios.*


// testing
fun main(){

    val compositeScenarioProgram = CompositeScenarioProgram()


    val scenarioProgramSumoLayer = createSumoScenarioProgram(
        "sumo-gui.exe",
        "C:\\Prog\\IntelliJ\\smlk4sumo\\src\\test\\resources\\pedestrian_crossing_car_to_x\\data\\run.sumocfg",
        0.1
    )
    val scenarioProgramV2XControllerInterfaceLayer = createControllerInterfaceScenarioProgram()

    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerPedestriansGreenUponPedestrianApproaching)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(pedestriansMustHaveGreenFor15Sec)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(pedestriansGreenMustOccurAfterPedestrianApproaching)


    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehicleNotifiesTLControllerOnApproach)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(triggerVehiclesGreenUponVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehiclesGreenMustOccurAfterVehicleRequest)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(vehiclesMustHaveGreenFor15Sec)

    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeVehicleApproachingEvent)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeTimeKeepingGreenForVehicleEvent)
    scenarioProgramV2XControllerInterfaceLayer.addGuaranteeScenario(visualizeVehicleControllerInputEvents)


    compositeScenarioProgram.addScenarioProgram(
        scenarioProgramSumoLayer,
        scenarioProgramV2XControllerInterfaceLayer
    )


    scenarioProgramV2XControllerInterfaceLayer.eventNode.registerSender(scenarioProgramSumoLayer.eventNode, sumoLayerOutputSymbolicEvents, sumoLayerTerminatingEvents)
    scenarioProgramSumoLayer.eventNode.registerSender(scenarioProgramV2XControllerInterfaceLayer.eventNode, sumoLayerInputSymbolicEvents)


    runBlocking {
        compositeScenarioProgram.run()
    }

}