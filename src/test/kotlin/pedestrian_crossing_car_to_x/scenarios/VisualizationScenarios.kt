package pedestrian_crossing_car_to_x.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightController
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.controllersinterface.Visualization
import org.scenariotools.smlk4sumo.sumo.urgent

val visualizeVehicleApproachingEvent = scenario(TrafficLightController::vehicleApproaching.symbolicEvent()) {
    val tlController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    val id = vehicleController.id + tlController.id

    urgent(
        Visualization.showArrow(
            id,
            "vehicleApproaching",
            vehicleController.positionX,
            vehicleController.positionY,
            tlController.xPos-5,
            tlController.yPos+8,
            stepsDuration = 20
        )
    )
}

val visualizeTimeKeepingGreenForVehicleEvent = scenario(VehicleController::timeKeepingGreen.symbolicEvent()) {
    val tlController = it.parameters[0] as TrafficLightController
    val vehicleController = it.receiver

    val sec = it.parameters[1] as Int

    val id = tlController.id + vehicleController.id

    urgent(
        Visualization.showArrow(
            id,
            "timeKeepingGreen($sec)",
            tlController.xPos -5,
            tlController.yPos + 8 + 1,
            vehicleController.positionX,
            vehicleController.positionY + 1,
            0,
            255,
            0,
            255,
            stepsDuration = 40
        )
    )
}

val visualizeVehicleControllerInputEvents = scenario(
    NonConcreteEventSet(
        VehicleController::enteredOuterTLPerimeter.symbolicEvent(),
        VehicleController::enteredInnerTLPerimeter.symbolicEvent(),
        VehicleController::exitedInnerTLPerimeter.symbolicEvent(),
        VehicleController::exitedOuterTLPerimeter.symbolicEvent()
    )
){
    val vehicleController = it.receiver as VehicleController
    val messageTypeName = it.type.name

    val id = vehicleController.id+messageTypeName

    urgent(
        Visualization.showArrow(
            id,
            messageTypeName,
            vehicleController.positionX,
            vehicleController.positionY-5,
            vehicleController.positionX,
            vehicleController.positionY,
            150,
            150,
            255,
            255,
            20
        )
    )
}