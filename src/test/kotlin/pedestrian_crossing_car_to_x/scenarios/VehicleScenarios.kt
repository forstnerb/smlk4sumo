package pedestrian_crossing_car_to_x.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightController
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightPhase
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.sumo.expect
import org.scenariotools.smlk4sumo.sumo.urgent
import org.scenariotools.smlk4sumo.sumo.waitForSimulationSteps

val vehicleNotifiesTLControllerOnApproach = scenario(VehicleController::enteredOuterTLPerimeter.symbolicEvent())
{
    val vehicleController = it.receiver
    val trafficLightController = it.parameters[0] as TrafficLightController

    urgent(trafficLightController.vehicleApproaching(vehicleController))

}

val triggerVehiclesGreenUponVehicleRequest = scenario(TrafficLightController::vehicleApproaching.symbolicEvent()) {

    val trafficLightController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    interruptingEvents.add(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)

    if (trafficLightController.currentPhase != TrafficLightPhase.vehicleGreen) {
        // if not green, then request green
        //println("*** requesting green for vehicles...")
        request(trafficLightController.turn(TrafficLightPhase.vehicleGreen))
        //println("*** requestED green for vehicles...")
    } else {
        // if green for vehicles for less than 15 seconds, keep green for cars, otherwise allow other transitionToPhase requests.
        if (trafficLightController.currentPhaseActiveSeconds <= 15) {
            interruptingEvents.add(trafficLightController.currentPhasedIsActiveFor(15))

            urgent(
                vehicleController.timeKeepingGreen(
                    trafficLightController,
                    15 - trafficLightController.currentPhaseActiveSeconds
                )
            )

            //println("***** keep green for vehicle ${vehicleController.id}...")
            forbiddenEvents.add(trafficLightController receives TrafficLightController::turn)
            waitFor(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)
        }
    }
}


val vehiclesGreenMustOccurAfterVehicleRequest = scenario(TrafficLightController::vehicleApproaching.symbolicEvent())
{

    val MAXSTEPSUNTILGREEN = 300 // 30 sec.

    val trafficLightController = it.receiver

    interruptingEvents.add(VehicleController::exitedInnerTLPerimeter.symbolicEvent() param trafficLightController)

    expect(
        { trafficLightController.currentPhase == TrafficLightPhase.vehicleGreen },
        MAXSTEPSUNTILGREEN,
        "Vehicles approaching the traffic light must have green after $MAXSTEPSUNTILGREEN steps."
    )
}

val vehiclesMustHaveGreenFor15Sec =
    scenario(TrafficLightController::phaseChanged.symbolicEvent() param TrafficLightPhase.vehicleGreen) {

        val minDelayBetweenGreenForVehicles = 150 // steps of 0.1 sec = 15 sec.

        val trafficLightController = it.receiver

        forbiddenEvents.add(trafficLightController receives TrafficLightController::turn)

        waitForSimulationSteps(minDelayBetweenGreenForVehicles)

    }


