package pedestrian_crossing_car_to_x.scenarios

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLight
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightController
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLightPhase
import org.scenariotools.smlk4sumo.sumo.expect
import org.scenariotools.smlk4sumo.sumo.waitForSimulationSteps

val triggerPedestriansGreenUponPedestrianApproaching
        = scenario(TrafficLightController::pedestrianApproachingCrossing.symbolicEvent()) {

        val trafficLightController = it.receiver
        interruptingEvents.add(trafficLightController.allPedestrianLeftCrossing())
        //println("### requesting green for pedestrians")
        request(trafficLightController.turn(TrafficLightPhase.pedestriansGreen))
        //println("### requestED green for pedestrians")
}

val pedestriansMustHaveGreenFor15Sec = scenario(
    TrafficLightController::phaseChanged.symbolicEvent() param TrafficLightPhase.pedestriansGreen) {

        val minDelayBetweenGreenForPedestrians = 150 // steps of 0.1 sec = 15 sec.

        val trafficLightController = it.receiver

        forbiddenEvents.add(trafficLightController.trafficLight receives TrafficLight::setPhase param ANY)

        waitForSimulationSteps(minDelayBetweenGreenForPedestrians)
}

val pedestriansGreenMustOccurAfterPedestrianApproaching = scenario(
    TrafficLightController::pedestrianApproachingCrossing.symbolicEvent())
{

        val MAXSTEPSUNTILGREEN = 300 // 30 sec

        val trafficLightController = it.receiver

        interruptingEvents.add(trafficLightController.allPedestrianLeftCrossing())

        expect({trafficLightController.currentPhase == TrafficLightPhase.pedestriansGreen},
                MAXSTEPSUNTILGREEN,
                "Pedestrians approaching the traffic light must have green after $MAXSTEPSUNTILGREEN steps.")
}

