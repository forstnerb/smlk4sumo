package org.scenariotools.smlk4sumo.controllersinterface.scenarios

import de.tudresden.ws.container.SumoPosition2D
import mu.KotlinLogging
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.controllersinterface.V2XControllersRoot
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.sumo.*

fun vehicleAppearedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoSimulation::vehicleAppeared.symbolicEvent()
)
{
    val id = it.parameters[0] as String
    urgent(v2xControllersRoot.addVehicleController(id))
}


fun vehicleDisappearedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoSimulation::vehicleDisAppeared.symbolicEvent()
)
{
    val id = it.parameters[0] as String
    urgent(v2xControllersRoot.removeVehicleController(id))
}

fun vehicleSpeedChangedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoVehicle::getSpeed.symbolicEvent()
)
{
    val sumoVehicle = it.receiver
    val speed1 = it.result()

    val speed2 = waitFor(sumoVehicle.getSpeed()).result()
    if (speed1 != speed2) {
        urgent(v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!.speedChanged(speed2))

        if(speed1 == 0.00) {
            urgent(v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!.resumed())
        }

        if(speed2 == 0.00) {
            val vehicleController = v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!
            urgent(vehicleController.brokeDown())
        }
    }
}


fun vehiclePositionChangedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoVehicle::getPosition.symbolicEvent()
)
{
    val sumoVehicle = it.receiver
    val pos1 = it.result()
    val pos2 = waitFor(sumoVehicle.getPosition()).result()

    if (!pos1.equalsPosition(pos2)) {
        urgent(
            v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!.positionChanged(pos1, pos2)
        )
    }
}


fun vehicleLaneChangedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    SumoVehicle::getLane.symbolicEvent()
)
{
    val laneID1 = it.result()
    val sumoVehicle = it.receiver
    val laneID2 = waitFor(sumoVehicle.getLane()).result()
    if (laneID1 != laneID2 || v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!.laneID == null) {
        urgent(v2xControllersRoot.vehicleControllers.get(sumoVehicle.id)!!.laneChanged(laneID2))
    }
}


fun vehiclePassingTLScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    VehicleController::positionChanged.symbolicEvent()
)
{

    val OUTER_PERIMITER = 90
    val INNER_PERIMITER = 5

    val previousPosition = it.parameters[0] as SumoPosition2D
    val currentPosition = it.parameters[1] as SumoPosition2D
    val vehicleController = it.receiver

    for (trafficLightController in v2xControllersRoot.trafficLightControllers.values) {
        val trafficLightPosition = SumoPosition2D(trafficLightController.xPos,trafficLightController.yPos)

        // entering outer TL perimeter
        if (trafficLightPosition.distance(previousPosition) > OUTER_PERIMITER && trafficLightPosition.distance(
                currentPosition
            ) <= OUTER_PERIMITER
        ) {
            urgent(
                vehicleController.enteredOuterTLPerimeter(trafficLightController)
            )
        }
        // entering inner TL perimeter
        if (trafficLightPosition.distance(previousPosition) > INNER_PERIMITER && trafficLightPosition.distance(
                currentPosition
            ) <= INNER_PERIMITER
        ) {
            urgent(
                vehicleController.enteredInnerTLPerimeter(trafficLightController)
            )
        }
        // exiting inner TL perimeter
        if (trafficLightPosition.distance(previousPosition) <= INNER_PERIMITER && trafficLightPosition.distance(
                currentPosition
            ) > INNER_PERIMITER
        ) {
            urgent(
                vehicleController.exitedInnerTLPerimeter(
                    trafficLightController
                )
            )
        }
        // exiting outer TL perimeter
        if (trafficLightPosition.distance(previousPosition) <= OUTER_PERIMITER && trafficLightPosition.distance(
                currentPosition
            ) > OUTER_PERIMITER
        ) {
            urgent(
                vehicleController.exitedOuterTLPerimeter(
                    trafficLightController
                )
            )
        }
    }


}

fun switchLanesScenario(v2xControllersRoot : V2XControllersRoot) = scenario(VehicleController::switchLane.symbolicEvent() ){

    val vehicleController = it.receiver
    val targetLane = it.parameters[0] as String
    val targetDuration = it.parameters[1] as Double

    request(vehicleController.vehicle.changeLane(targetLane, targetDuration))
}

fun setSpeedScenario(v2xControllersRoot: V2XControllersRoot) = scenario(VehicleController::setSpeed.symbolicEvent()){

    val vehicleController = it.receiver
    val speed = it.parameters[0] as Double

    request(vehicleController.vehicle.setSpeed(speed))
}

fun setLaneChangeModeScenario(v2xControllersRoot: V2XControllersRoot) = scenario(VehicleController::setLCM.symbolicEvent()){

    val vehicleController = it.receiver
    val mode = it.parameters[0] as Int

    request(vehicleController.vehicle.setLaneChangeMode(mode))
}