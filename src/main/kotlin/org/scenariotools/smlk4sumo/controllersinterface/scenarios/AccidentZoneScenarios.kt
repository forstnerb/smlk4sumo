package org.scenariotools.smlk4sumo.controllersinterface.scenarios

import de.tudresden.ws.container.SumoPosition2D
import mu.KotlinLogging
import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.AccidentZoneController
import org.scenariotools.smlk4sumo.controllersinterface.AccidentZoneControllerVariables
import org.scenariotools.smlk4sumo.controllersinterface.V2XControllersRoot
import org.scenariotools.smlk4sumo.controllersinterface.VehicleController
import org.scenariotools.smlk4sumo.sumo.*

private val logger = KotlinLogging.logger {}

fun accidentZoneControllerCreationScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    VehicleController::brokeDown.symbolicEvent()
)
{
    val vehicleController = it.receiver
    val vehicleId = vehicleController.id

    //only vehicles which are planned to break down (and thus named v0/v1/..) in the routes-file are allowed to create an AZC
    if (vehicleId.first() == 'v') {

        val accidentZonePosition = SumoPosition2D(vehicleController.positionX, vehicleController.positionY)

        // extract the planned stop-parameters (if possible) from vehiclename
        // based on the format/regex  X9****9.99 bzw. Y9****9.99
        val plannedStopPosX = (vehicleId.subSequence(vehicleId.indexOf('X',0)+1,
            vehicleId.indexOf('.',vehicleId.indexOf('X',0))+3) as String).toDoubleOrNull()

        val plannedStopPosY = (vehicleId.subSequence(vehicleId.indexOf('Y',0)+1,
            vehicleId.indexOf('.',vehicleId.indexOf('Y',0))+3) as String).toDoubleOrNull()

        // check if this is near the planned stop-point for the vehicle (if so - create an AZC)
        if(plannedStopPosX != null && plannedStopPosY != null
            && accidentZonePosition.distance(plannedStopPosX,plannedStopPosY) < AccidentZoneControllerVariables.MAX_PLANNED_STOP_DISTANCE) {

            // get the edge without the specific lane-number (eg. EW instead of EW_0)
            val lane = vehicleController.laneID!!.substring(0,vehicleController.laneID!!.indexOf('_'))

            logger.debug { "AZC created with:" }
            logger.debug { "Id: " + vehicleController.id + "C , xPos: " + vehicleController.positionX + " , yPos: " + vehicleController.positionY + " , Lane: " + lane}
            logger.debug {
                "Distance to the planned point of stop: " + accidentZonePosition.distance(
                    plannedStopPosX,
                    plannedStopPosY
                )
            }

            urgent(
                v2xControllersRoot.addAccidentZoneController(
                    vehicleController.id + "C",
                    vehicleController.positionX,
                    vehicleController.positionY,
                    vehicleController,
                    lane
                )
            )

            val accidentZoneController = v2xControllersRoot.accidentZoneControllers.get(vehicleController.id + "C")!!

            // snapshot the list of vehicles at this moment
            val vehicleControllersSnapshot: List<VehicleController> = (v2xControllersRoot.vehicleControllers.values).toList()

            // look for vehicles on the same edge somewhat close to the accident
            for (v in vehicleControllersSnapshot) {

                if (accidentZoneController.lane == v.laneID!!.substring(0,v.laneID!!.indexOf('_')) &&
                    accidentZonePosition.distance(v.positionX, v.positionY) <= AccidentZoneControllerVariables.DANGER_PERIMETER &&
                    accidentZonePosition.distance(v.positionX, v.positionY) > AccidentZoneControllerVariables.STOP_PERIMETER
                ) {
                    urgent(accidentZoneController.addingInitialVehicleInDangerZone(v))
                }
            }

            request(accidentZoneController.sortVehicleQueueByDistance())

            val vSnapshot: List<VehicleController> = (accidentZoneController.vehiclesInQueue).toList()
            logger.debug { "Sorted vehicleQueue:" }
            for (vtest in vSnapshot) {
                logger.debug {
                    vtest.id + " with distance " + accidentZonePosition.distance(
                        vtest.positionX,
                        vtest.positionY
                    )
                }
            }
        }
    }
}

fun accidentZoneControllerRemovalScenario(v2xControllersRoot: V2XControllersRoot) = scenario(
    VehicleController::resumed.symbolicEvent()
)
{
    val vehicleController = it.receiver

    val accidentZoneController = v2xControllersRoot.accidentZoneControllers.get(vehicleController.id + "C")

    if(accidentZoneController != null) {
        logger.debug{"Vehicle " + vehicleController.id + " is resuming now and should remove " + accidentZoneController.id + " , the queue at this point of time:"}
        for (v in accidentZoneController.vehiclesInQueue){
            logger.debug{v.id}
        }

        urgent(v2xControllersRoot.removeAccidentZoneController(accidentZoneController.id))

        val accidentZonePosition = SumoPosition2D(accidentZoneController.xPos, accidentZoneController.yPos)
        var signalCounter = 0

        // snapshot the list of vehicles at this moment
        val vehicleControllersSnapshot: List<VehicleController> = (v2xControllersRoot.vehicleControllers.values).toList()

        for (vC in vehicleControllersSnapshot) {

            // if the vehicle is in the danger-zone, let it speed up
            if( accidentZoneController.lane == vC.laneID!!.substring(0,vC.laneID!!.indexOf('_')) &&
                accidentZonePosition.distance(vC.positionX,vC.positionY) < AccidentZoneControllerVariables.DANGER_PERIMETER) {
                request(accidentZoneController.vehicleExitedDangerZone(vC))
                signalCounter++

                // if the vehicle is in the vehicleQueue let it change lanes by itself again (so not past the accident, these vehicles already do)
                if (accidentZoneController.vehiclesInQueue.contains(vC)) {
                    request(vC.setLCM(-1))
                    signalCounter++
                }

            }

            // limit the outgoing events per simulation-step
            if(signalCounter > AccidentZoneControllerVariables.MAX_SIGNAL_COUNTER){

                waitForSimulationSteps(1)
                signalCounter = 0
            }
        }
    }
}


fun vehiclePassingAccidentZoneController(v2xControllersRoot: V2XControllersRoot) = scenario(
    VehicleController::positionChanged.symbolicEvent()
)
{
    val vehicleController = it.receiver
    val previousPosition = it.parameters[0] as SumoPosition2D
    val currentPosition = it.parameters[1] as SumoPosition2D

    // snapshot the list of AZCs at this moment
    val accidentZoneControllersSnapshot: List<AccidentZoneController> = (v2xControllersRoot.accidentZoneControllers.values).toList()

    for (accidentZoneController in accidentZoneControllersSnapshot) {

        // check if the vehicle is on the same edge as the azc
        if(accidentZoneController.lane == vehicleController.laneID!!.substring(0,vehicleController.laneID!!.indexOf('_'))){

            val accidentZonePosition = SumoPosition2D(accidentZoneController.xPos, accidentZoneController.yPos)

            // check if entering DangerZone
            if (accidentZonePosition.distance(previousPosition) > AccidentZoneControllerVariables.DANGER_PERIMETER && accidentZonePosition.distance(
                    currentPosition
                ) <= AccidentZoneControllerVariables.DANGER_PERIMETER
            ) {
                urgent(
                    accidentZoneController.vehicleEnteredDangerZone(vehicleController)
                )
            }

            // check if leaving DangerZone
            if (accidentZonePosition.distance(previousPosition) <= AccidentZoneControllerVariables.FREE_PERIMETER && accidentZonePosition.distance(
                    currentPosition
                ) > AccidentZoneControllerVariables.FREE_PERIMETER
            ) {
                urgent(
                    accidentZoneController.vehicleExitedDangerZone(vehicleController)
                )
            }

            // check if entering StopArea
            if (accidentZonePosition.distance(previousPosition) > AccidentZoneControllerVariables.STOP_PERIMETER && accidentZonePosition.distance(
                    currentPosition
                ) <= AccidentZoneControllerVariables.STOP_PERIMETER
            ) {
                if (vehicleController.id == accidentZoneController.vehiclesInQueue[0].id) {
                    urgent(
                        accidentZoneController.vehicleEnteredEmptyStopArea(vehicleController)
                    )
                }
                else {
                    urgent(
                        accidentZoneController.vehicleEnteredFilledStopArea(vehicleController)
                    )
                    request(
                        accidentZoneController.handleQueue()
                    )
                }
            }
        }

    }
}

fun addingInitialVehicleInDangerZoneScenario() = scenario(
    AccidentZoneController::addingInitialVehicleInDangerZone.symbolicEvent()
)
{
    val accidentZoneController = it.receiver
    val vehicleController = it.parameters[0] as VehicleController

    val accidentVehiclePosition = SumoPosition2D(accidentZoneController.xPos,accidentZoneController.yPos)
    val pos1 = SumoPosition2D(vehicleController.positionX, vehicleController.positionY)

    forbiddenEvents.add(accidentZoneController.sortVehicleQueueByDistance())

    waitForSimulationSteps(1)

    val pos2 = SumoPosition2D(vehicleController.positionX, vehicleController.positionY)

    // check if the vehicle is on the way to the accident and not past it
    if (accidentVehiclePosition.distance(pos1) >= accidentVehiclePosition.distance(pos2)) {
        urgent(accidentZoneController.vehicleEnteredDangerZone(vehicleController))
    }
}

fun vehicleEnteredEmptyStopAreaScenario() = scenario(
    AccidentZoneController::vehicleEnteredEmptyStopArea.symbolicEvent()
)
    {
        val accidentZoneController = it.receiver
        val vehicleController = it.parameters[0] as VehicleController

        forbiddenEvents.add(accidentZoneController.handleQueue())

        waitForSimulationSteps(AccidentZoneControllerVariables.WAITING_TIME)

        urgent(accidentZoneController.removeVehicleFromQueue(vehicleController))
    }

fun handleQueueScenario() = scenario(
    AccidentZoneController::handleQueue.symbolicEvent()
)
{
    val accidentZoneController = it.receiver

    forbiddenEvents.add(accidentZoneController.handleQueue())

    val vehicleController = accidentZoneController.vehiclesInQueue.first()

    urgent(accidentZoneController.giveVehiclePermissionToPass(vehicleController))

    waitForSimulationSteps(AccidentZoneControllerVariables.WAITING_TIME)

    urgent(accidentZoneController.removeVehicleFromQueue(vehicleController))
}