package org.scenariotools.smlk4sumo.controllersinterface

import de.tudresden.ws.container.SumoPosition2D
import org.scenariotools.smlk.event
import kotlin.math.absoluteValue
import org.scenariotools.smlk4sumo.sumo.*

class V2XControllersRoot() {

    val vehicleControllers: MutableMap<String, VehicleController> = mutableMapOf()
    fun addVehicleController(id: String) = event(id) {
        val vehicleController = VehicleController(id)
        vehicleControllers.put(id, vehicleController)
        vehicleController
    }

    fun removeVehicleController(id: String) = event(id) {
        vehicleControllers.remove(id)
    }

    val trafficLightControllers: MutableMap<String, TrafficLightController> = mutableMapOf()
    fun addTrafficLightController(id: String, xPos : Double, yPos: Double) = event(id, xPos, yPos) {
        val trafficLightController = TrafficLightController(id, xPos = xPos, yPos = yPos)
        trafficLightControllers.put(id, trafficLightController)
        trafficLightController
    }

    val accidentZoneControllers: MutableMap<String, AccidentZoneController> = mutableMapOf()
    fun addAccidentZoneController(id: String, xPos : Double, yPos: Double, vehicleController: VehicleController, lane: String) = event(id, xPos, yPos, vehicleController, lane) {
        val vehiclesInQueue: MutableList<VehicleController> = mutableListOf()
        val accidentZoneController = AccidentZoneController(id, vehiclesInQueue, xPos = xPos, yPos = yPos, vehicleController = vehicleController, lane = lane)
        accidentZoneControllers.put(id, accidentZoneController)
        accidentZoneController
    }

    fun removeAccidentZoneController(id: String) = event(id) {
        accidentZoneControllers.remove(id)
    }

}


class VehicleController(val id : String, var laneID: String? = null, var positionX : Double = 0.0, var positionY : Double = 0.0, var speed : Double = 0.0){
    fun speedChanged(speed : Double) = event(speed){this.speed = speed}
    fun positionChanged(oldPosition : SumoPosition2D, newPosition : SumoPosition2D) = event(oldPosition, newPosition){
        this.positionX = newPosition.x
        this.positionY = newPosition.y
    }
    fun laneChanged(laneID : String) = event(laneID){this.laneID = laneID}

    fun enteredOuterTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun exitedOuterTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun enteredInnerTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }
    fun exitedInnerTLPerimeter(trafficLightController : TrafficLightController) = event(trafficLightController){   }

    fun timeKeepingGreen(trafficLightController : TrafficLightController, sec : Int) = event(trafficLightController, sec){   }

    override fun toString() = "VehicleController(id=$id)"

    fun switchLane(targetLane: String, targetDuration: Double) = event(targetLane,targetDuration){   }
    fun setSpeed(speed: Double) = event(speed){   }
    fun setLCM(mode: Int) = event(mode){}

    fun brokeDown() = event{   }
    fun resumed() = event{  }

    val vehicle = Vehicle(id)
}

class Vehicle(val id: String){
    fun changeLane(targetLane : String, targetDuration : Double) = event(targetLane, targetDuration) {}
    fun setSpeed(speed : Double) = event(speed){}
    fun setLaneChangeMode(mode: Int) = event(mode){}
}

class TrafficLightController(val id : String, var currentPhase : Int = 0, val xPos : Double, val yPos: Double){

    val trafficLight = TrafficLight(id)

    fun phaseChanged(phase : Int) = event(phase) {this.currentPhase = phase}

    fun vehicleApproaching(vehicleController: VehicleController) = event(vehicleController) {}

    var pedestrianCounter = 0

    fun pedestrianApproachingCrossing() = event {pedestrianCounter++}

    fun pedestrianLeftCrossing() = event {pedestrianCounter--}

    fun allPedestrianLeftCrossing() = event{}

    fun turn(tlPhase : Int) = event(tlPhase){}

    var currentPhaseActiveSeconds = 0
    fun currentPhasedIsActiveFor(sec : Int) = event(sec){currentPhaseActiveSeconds = sec}


    override fun toString() = "TrafficLightController(id=$id)"
}

class TrafficLight(val id : String){
    fun setPhase(tlPhase : Int) = event (tlPhase) {}
}

object Visualization{
    fun showArrow(id : String, labelText : String, x1 : Double, y1 : Double, x2 : Double, y2 : Double, rgbaRED:Int=255, rgbaGREEN:Int=255, rgbaBLUE:Int=0, rgbaAlpha:Int=255, stepsDuration : Int) = event(id, labelText, x1, y1, x2, y2, rgbaRED, rgbaGREEN, rgbaBLUE, rgbaAlpha, stepsDuration){}
}

object TrafficLightPhase{
    val vehicleGreen = 0
    val vehicleYellow = 1
    val pedestriansGreen = 2
    val vehicleAndPedestriansRed = 3
}

class AccidentZoneController(val id: String, val vehiclesInQueue: MutableList<VehicleController>, val xPos: Double, val yPos: Double, val vehicleController: VehicleController, val lane: String){

    fun vehicleEnteredDangerZone(vehicleController: VehicleController) = event (vehicleController) {
        this.vehiclesInQueue.add(vehicleController)
    }

    fun vehicleExitedDangerZone(vehicleController: VehicleController) = event (vehicleController) {}


    fun removeVehicleFromQueue(vehicleController: VehicleController) = event (vehicleController) {
        this.vehiclesInQueue.remove(vehicleController)
    }

    fun addingInitialVehicleInDangerZone(vehicleController: VehicleController) = event (vehicleController) {  }

    fun sortVehicleQueueByDistance() = event{
        vehiclesInQueue.sortBy({ (SumoPosition2D(this.xPos,this.yPos).distance(it.positionX,it.positionY)).absoluteValue })
    }

    fun vehicleEnteredEmptyStopArea(vehicleController: VehicleController) = event(vehicleController) {}

    fun vehicleEnteredFilledStopArea(vehicleController: VehicleController) = event(vehicleController) {}

    fun handleQueue() = event {}

    fun giveVehiclePermissionToPass(vehicleController: VehicleController) = event(vehicleController) {}

}

object AccidentZoneControllerVariables{
    val DANGER_PERIMETER = 100          // where the zone of danger of the accident begins (and vehicles slow down & dont change lanes anymore)
    val STOP_PERIMETER = 30             // how far the vehicles should stop before the accident (when waiting for the permission to pass)
    val FREE_PERIMETER = 50             // how far after the accident the vehicle can drive normally again
    val WAITING_TIME = 30               // how much time to wait before giving the next vehicle the permission to pass the accident

    val MAX_PLANNED_STOP_DISTANCE = 20  // how far from the variables given in its name is an vehicle allowed to create an AZC when stopping
    val MAX_SIGNAL_COUNTER = 20         // after how many sent events to vehicles will the azc-creator wait a simulation-step (to not overburden smlk/sumo)
}
