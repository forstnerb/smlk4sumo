package org.scenariotools.smlk4sumo.controllersinterface

import org.scenariotools.smlk.*
import org.scenariotools.smlk4sumo.controllersinterface.scenarios.*
import org.scenariotools.smlk4sumo.sumo.SumoPerson
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.SumoTrafficLight
import org.scenariotools.smlk4sumo.sumo.SumoVehicle
import kotlin.reflect.KFunction

fun createControllerInterfaceScenarioProgram() : ScenarioProgram {

    val scenarioProgram = ScenarioProgram("V2XControllerInterfaceLayer", terminatingEvents = mutableSetOf(SumoSimulation::close.symbolicEvent()))

    val v2xControllersRoot = V2XControllersRoot()
    scenarioProgram.addGuaranteeScenario(vehicleAppearedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehicleDisappearedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehicleSpeedChangedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehiclePositionChangedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehicleLaneChangedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(tlControllerCreationScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(tlPhaseChangedScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(tlControllerDetectsPedestrianScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehiclePassingTLScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(manageTLPhaseTransitioning())
    scenarioProgram.addGuaranteeScenario(countTLPhaseActiveDuration())
    scenarioProgram.addGuaranteeScenario(switchLanesScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(setSpeedScenario(v2xControllersRoot))


    scenarioProgram.addGuaranteeScenario(accidentZoneControllerCreationScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(vehiclePassingAccidentZoneController(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(accidentZoneControllerRemovalScenario(v2xControllersRoot))
    scenarioProgram.addGuaranteeScenario(addingInitialVehicleInDangerZoneScenario())
    scenarioProgram.addGuaranteeScenario(vehicleEnteredEmptyStopAreaScenario())
    scenarioProgram.addGuaranteeScenario(handleQueueScenario())
    scenarioProgram.addGuaranteeScenario(setLaneChangeModeScenario(v2xControllersRoot))



    scenarioProgram.addEnvironmentMessageType(*sumoLayerOutputEventTypes)

    return scenarioProgram
}

val sumoLayerOutputEventTypes = arrayOf(
    SumoSimulation::start,
    SumoSimulation::timestep,
    SumoSimulation::close,
    SumoSimulation::vehicleAppeared,
    SumoSimulation::vehicleDisAppeared,
    SumoSimulation::personAppeared,
    SumoSimulation::personDisAppeared,
    SumoVehicle::getLane,
    SumoVehicle::getPosition,
    SumoVehicle::getSpeed,
    SumoVehicle::getLaneChangeMode,
    SumoPerson::changedEdgeTo,
    SumoTrafficLight::getPhase
) as Array<KFunction<ObjectEvent<Any,Any?>>>
val sumoLayerOutputSymbolicEvents = setOf(*sumoLayerOutputEventTypes).symbolicEvents()

val sumoLayerTerminatingEvents = SumoSimulation::close.symbolicEvent()

val sumoLayerInputEventTypes = arrayOf(
    TrafficLight::setPhase,
    Visualization::showArrow,
    Vehicle::changeLane,
    Vehicle::setSpeed,
    Vehicle::setLaneChangeMode
) as Array<KFunction<ObjectEvent<Any,Any?>>>
val sumoLayerInputSymbolicEvents = setOf(*sumoLayerInputEventTypes).symbolicEvents()


