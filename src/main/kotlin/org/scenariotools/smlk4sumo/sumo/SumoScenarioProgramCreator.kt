package org.scenariotools.smlk4sumo.sumo

import it.polito.appeal.traci.SumoTraciConnection
import org.scenariotools.smlk.ScenarioProgram
import org.scenariotools.smlk4sumo.controllersinterface.TrafficLight
import org.scenariotools.smlk4sumo.controllersinterface.Visualization
import org.scenariotools.smlk4sumo.sumo.scenarios.*

fun createSumoScenarioProgram(sumo_bin:String, config_file:String, step_length_sec:Double) : ScenarioProgram{

    val sumoTraciConnection = SumoTraciConnection(sumo_bin, config_file)
    sumoTraciConnection.addOption("step-length", step_length_sec.toString())
    val sumoSimulation = SumoSimulation(sumoTraciConnection)

    val scenarioProgram = ScenarioProgram("SumoLayer")

    scenarioProgram.addAssumptionScenario{
        request(sumoSimulation.start())
        do {
            request(sumoSimulation.timestep())
        } while (sumoSimulation.getMinExpectedNumber() > 0)
        request(sumoSimulation.close())
    }

    scenarioProgram.addEnvironmentMessageType(
        SumoSimulation::start,
        SumoSimulation::timestep,
        SumoSimulation::close,
        TrafficLight::setPhase,
        Visualization::showArrow
    )


    scenarioProgram.addGuaranteeScenario(vehiclesAppearanceDisappearanceDetector(sumoSimulation))
    scenarioProgram.addGuaranteeScenario(vehiclesPositionAndSpeedMaintainer(sumoSimulation))

    scenarioProgram.addGuaranteeScenario(trafficLightPhaseMonitor(sumoSimulation))
    scenarioProgram.addGuaranteeScenario(trafficLightPhaseChange(sumoSimulation))

    scenarioProgram.addGuaranteeScenario(personsMaintainer(sumoSimulation))
    scenarioProgram.addGuaranteeScenario(personsEdgeMaintainer(sumoSimulation))

    scenarioProgram.addGuaranteeScenario(showArrow(sumoSimulation))

    scenarioProgram.addGuaranteeScenario(vehicleLaneChange(sumoSimulation))
    scenarioProgram.addGuaranteeScenario(vehicleSpeedSetter(sumoSimulation))

    scenarioProgram.addGuaranteeScenario(vehicleLaneChangeModeSetter(sumoSimulation))

    return scenarioProgram
}
