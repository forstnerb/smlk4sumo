package org.scenariotools.smlk4sumo.sumo.scenarios

import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.controllersinterface.Visualization
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent
import org.scenariotools.smlk4sumo.sumo.waitForSimulationSteps

fun showArrow(sumoSimulation: SumoSimulation) = scenario(Visualization::showArrow.symbolicEvent()) {
    val stepsDuration = it.parameters[10] as Int
    val id = (it.parameters[0] as String) + sumoSimulation.currentTimeStep
    urgent(sumoSimulation.addArrow(
        id,
        it.parameters[1] as String,
        it.parameters[2] as Double,
        it.parameters[3] as Double,
        it.parameters[4] as Double,
        it.parameters[5] as Double,
        it.parameters[6] as Int,
        it.parameters[7] as Int,
        it.parameters[8] as Int,
        it.parameters[9] as Int
    ))
    waitForSimulationSteps(stepsDuration)
    urgent(sumoSimulation.removeArrow(id))
}
