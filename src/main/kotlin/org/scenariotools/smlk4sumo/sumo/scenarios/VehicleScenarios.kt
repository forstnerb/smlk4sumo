package org.scenariotools.smlk4sumo.sumo.scenarios

import mu.KotlinLogging
import org.scenariotools.smlk.scenario
import org.scenariotools.smlk.symbolicEvent
import org.scenariotools.smlk4sumo.controllersinterface.Vehicle
import org.scenariotools.smlk4sumo.sumo.SumoSimulation
import org.scenariotools.smlk4sumo.sumo.urgent

fun vehiclesAppearanceDisappearanceDetector(sumoSimulation: SumoSimulation) = scenario {
    do {
        waitFor(sumoSimulation.timestep())

        // Note: "departed" are vehicles that started their route; "arrived" are those that arrived at the end of their route
        for (id in sumoSimulation.getDepartedIDList()) {
            urgent(sumoSimulation.vehicleAppeared(id))
        }
        for (id in sumoSimulation.getArrivedIDList()) {
            urgent(sumoSimulation.vehicleDisAppeared(id))
        }
        urgent(sumoSimulation.vehicleListUpdated())
    } while (!sumoSimulation.closed)
}

fun vehiclesPositionAndSpeedMaintainer(sumoSimulation: SumoSimulation) = scenario {
    do {
        waitFor(sumoSimulation.vehicleListUpdated())
        for (sumoVehicle in sumoSimulation.vehicles.values) {
            urgent(sumoVehicle.getLane())
            urgent(sumoVehicle.getPosition())
            urgent(sumoVehicle.getSpeed())
        }
    } while (!sumoSimulation.closed)
}

fun vehicleLaneChange(sumoSimulation: SumoSimulation) = scenario(Vehicle::changeLane.symbolicEvent()){
    val vehicle = it.receiver
    val targetLane = it.parameters[0] as String
    val targetDuration = it.parameters[1] as Double

    val sumoVehicle = sumoSimulation.vehicles[vehicle.id]!!
    urgent(sumoVehicle.changeLane(targetLane, targetDuration))
}

fun vehicleSpeedSetter(sumoSimulation : SumoSimulation) = scenario(Vehicle::setSpeed.symbolicEvent()){
    val vehicle = it.receiver
    val speed = it.parameters[0] as Double

    val sumoVehicle = sumoSimulation.vehicles[vehicle.id]!!
    urgent(sumoVehicle.setSpeed(speed))
}

fun vehicleLaneChangeModeSetter(sumoSimulation: SumoSimulation) = scenario(Vehicle::setLaneChangeMode.symbolicEvent()){

    val vehicle = it.receiver
    val newMode = it.parameters[0] as Int

    // if the value is "-1", interpret it as a command to set a changed value back to before
    if(newMode != -1) {
        val sumoVehicle = sumoSimulation.vehicles[vehicle.id]!!

        // save the old LaneChangeMode-value
        val oldMode = urgent(sumoVehicle.getLaneChangeMode()).result()

        // change the mode to the new value
        request(sumoVehicle.setLaneChangeMode(newMode))

        // wait for the command to reset to the old LaneChangeMode
        waitFor(vehicle.setLaneChangeMode(-1))
        request(sumoVehicle.setLaneChangeMode(oldMode))
    }
}