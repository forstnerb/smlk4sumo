package org.scenariotools.smlk4sumo.sumo

import de.tudresden.sumo.cmd.*
import de.tudresden.sumo.util.SumoCommand
import de.tudresden.ws.container.*
import de.tudresden.sumo.config.Constants
import it.polito.appeal.traci.SumoTraciConnection
import mu.KotlinLogging
import org.scenariotools.smlk.event
import java.awt.geom.AffineTransform


private val logger = KotlinLogging.logger {}

abstract class SumoSimulationElement(val id : String, val sumoSimulation: SumoSimulation, val sumoTraciConnection:SumoTraciConnection){
    override fun toString() = "${this::class.simpleName}_$id"
}


class SumoSimulation(val sumoTraciConnection:SumoTraciConnection){

    init {
        sumoTraciConnection.runServer()
        sumoTraciConnection.do_timestep() // need to perform a time step before the simulation elements can be retrieved.
    }

    var closed = false

    fun start() = event(){}

    fun close() = event{
        sumoTraciConnection.close()
        closed = true
    }

    var currentTimeStep = 0

    fun timestep() = event{
        sumoTraciConnection.do_timestep();
        currentTimeStep = sumoTraciConnection.do_job_get(Simulation.getCurrentTime()) as Int
        currentTimeStep
    }

    fun getMinExpectedNumber() = sumoTraciConnection.do_job_get(Simulation.getMinExpectedNumber()) as Int

    val junctions : Map<String, SumoJunction> = (sumoTraciConnection.do_job_get(Junction.getIDList()) as SumoStringList).map { it to SumoJunction(
        this,
        it
    )
    }.toMap()
    val edges : Map<String, SumoEdge> = (sumoTraciConnection.do_job_get(Edge.getIDList()) as SumoStringList).map { it to SumoEdge(
        this,
        it
    )
    }.toMap()
    val lanes : Map<String, SumoLane> = (sumoTraciConnection.do_job_get(Lane.getIDList()) as SumoStringList).map { it to SumoLane(
        this,
        it,
        edges[sumoTraciConnection.do_job_get(Lane.getEdgeID(it)) as String]!!
    )
    }.toMap()
    val trafficLights : Map<String, SumoTrafficLight> = (sumoTraciConnection.do_job_get(Trafficlight.getIDList()) as SumoStringList).map { it to SumoTrafficLight(
        this,
        it
    )
    }.toMap()

    val vehicles : MutableMap<String, SumoVehicle> = mutableMapOf()
    val persons : MutableMap<String, SumoPerson> = mutableMapOf()


    // Note: "departed" are vehicles that started their route; "arrived" are those that arrived at the end of their route
    fun getDepartedIDList() = sumoTraciConnection.do_job_get(Simulation.getDepartedIDList()) as SumoStringList
    fun getArrivedIDList() = sumoTraciConnection.do_job_get(Simulation.getArrivedIDList()) as SumoStringList

    fun vehicleAppeared(id:String) = event(id){
        val newVehicle = SumoVehicle(this, id)
        vehicles.put(id, newVehicle)
        newVehicle
    }
    fun vehicleDisAppeared(id:String) = event(id){
        vehicles.remove(id)
    }
    fun vehicleListUpdated() = event{}

    fun getPersonsIDList() = sumoTraciConnection.do_job_get(Person.getIDList()) as SumoStringList
    fun personAppeared(id:String) = event(id){
        val newPerson = SumoPerson(this, id)
        persons.put(id, newPerson)
        newPerson
    }
    fun personDisAppeared(id:String) = event(id) {
        persons.remove(id)
    }
    fun personListUpdated() = event {}


    fun addArrow(id : String, labelText : String, x1 : Double, y1 : Double, x2 : Double, y2 : Double, rgbaRED:Int=255, rgbaGREEN:Int=255, rgbaBLUE:Int=0, rgbaAlpha:Int=255) = event(id, labelText, x1, y1, x2, y2, rgbaRED, rgbaGREEN, rgbaBLUE, rgbaAlpha){
        sumoTraciConnection.do_job_set(Polygon.add(id, createArrowGeometry(x1, x2, y1, y2), SumoColor(rgbaRED,rgbaGREEN,rgbaBLUE,rgbaAlpha), true, labelText, 10))
    }
    fun removeArrow(id : String) = event(id) {
        sumoTraciConnection.do_job_set(Polygon.remove(id,10))
    }


    override fun toString() = "SumoSimulation"

}


class SumoJunction(sumoSimulation: SumoSimulation, id:String) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){
    val position = sumoTraciConnection.do_job_get(Junction.getPosition(id)) as SumoPosition2D

}

class SumoEdge(sumoSimulation: SumoSimulation, id:String) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){
    fun getLastStepPersonIDs() = sumoTraciConnection.do_job_get(Edge.getLastStepPersonIDs(id)) as SumoStringList
}

class SumoLane(sumoSimulation: SumoSimulation, id:String, val edge: SumoEdge) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){}


class SumoTrafficLight(sumoSimulation: SumoSimulation, id:String) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun getPhase() = event {
        sumoTraciConnection.do_job_get(Trafficlight.getPhase(id)) as Int
    }

    fun setPhase(phase : Int) = event(phase) { sumoTraciConnection.do_job_set(Trafficlight.setPhase(id,phase)) }

    fun getCoordinates(sumoSimulation : SumoSimulation): Pair<Double, Double> {
        val correspondingJunction = sumoSimulation.junctions[this.id]
        if (correspondingJunction != null) {
            return correspondingJunction.position.x to correspondingJunction.position.y
        }else{
            return 0.0 to 0.0
        }
    }

}

object TrafficLightPhase{
    val vehicleGreen = 0
    val vehicleYellow = 1
    val pedestriansGreen = 2
    val vehicleAndPedestriansRed = 3
}

class SumoVehicle(sumoSimulation: SumoSimulation, id:String) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){

    fun getPosition() = event {
        sumoTraciConnection.do_job_get(Vehicle.getPosition(id)) as SumoPosition2D
    }

    fun getSpeed() = event {
        sumoTraciConnection.do_job_get(Vehicle.getSpeed(id)) as Double
    }

    fun getLane() = event {
        sumoTraciConnection.do_job_get(Vehicle.getLaneID(id)) as String
    }

    fun changeLane(targetLane: String, targetDuration: Double) = event(targetLane, targetDuration){
        val array = arrayOf(targetLane.toByte(),targetDuration)
        val command = SumoCommand(Constants.CMD_SET_VEHICLE_VARIABLE, Constants.CMD_CHANGELANE, id, array)
        sumoTraciConnection.do_job_set(command)
    }

    fun setSpeed(speed: Double) = event (speed){
        sumoTraciConnection.do_job_set((Vehicle.setSpeed(id, speed)))
    }

    fun getLaneChangeMode() = event {
        sumoTraciConnection.do_job_get(Vehicle.getLaneChangeMode(id)) as Int
    }

    fun setLaneChangeMode(mode: Int) = event (mode) {
        sumoTraciConnection.do_job_set(Vehicle.setLaneChangeMode(id, mode))
    }
}


class SumoPerson(sumoSimulation: SumoSimulation, id:String, var edge: SumoEdge? = null) : SumoSimulationElement(id, sumoSimulation, sumoSimulation.sumoTraciConnection){

    var previousEdge : SumoEdge? = null

    fun changedEdgeTo(newEdge : SumoEdge) = event(newEdge) {
        logger.debug { "Person $id changed to edge ${newEdge.id}" }
        previousEdge = edge
        edge = newEdge
    }

}


fun SumoPosition2D.distance(x:Double, y:Double) = Math.hypot(this.x-x, this.y-y)
fun SumoPosition2D.distance(sumoPosition2D: SumoPosition2D) = distance(sumoPosition2D.x, sumoPosition2D.y)
fun SumoPosition2D.copy() = SumoPosition2D(this.x,this.y)
fun SumoPosition2D.equalsPosition(sp2D: SumoPosition2D) = (sp2D.x == x && sp2D.y == y)
fun SumoPosition2D.updatePosition(sp2D: SumoPosition2D) { x = sp2D.x ; y = sp2D.y }



fun createArrowGeometry(x1: Double, x2:Double, y1:Double, y2:Double, thickness : Double = 0.25, arrowHatSize : Double = 5.0) : SumoGeometry{
    val distance = Math.hypot(x2-x1, y2-y1)

    val pointArray = arrayOf (
        -thickness,0.0,
        -thickness, distance - arrowHatSize*thickness,
        -arrowHatSize*thickness, distance - 2*arrowHatSize*thickness,
        0.0,  distance,
        arrowHatSize*thickness, distance-2*arrowHatSize*thickness,
        thickness, distance-arrowHatSize*thickness,
        thickness,0.0
    ).toDoubleArray()

    val translateAndRotate = AffineTransform()
    translateAndRotate.setToIdentity()
    val angle = Math.atan2(y2 - y1, x2 - x1)
    translateAndRotate.translate(x1, y1)
    translateAndRotate.rotate(angle - Math.PI / 2.0)

    translateAndRotate.transform(pointArray, 0 , pointArray, 0, 7)

    val sumoGeometry = SumoGeometry()

    for (i in 0..12 step 2){
        sumoGeometry.add(SumoPosition2D(pointArray[i], pointArray[i+1]))
    }

    return sumoGeometry
}